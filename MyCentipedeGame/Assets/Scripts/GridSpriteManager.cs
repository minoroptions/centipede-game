using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSpriteManager : MonoBehaviour
{
    private class SpriteUsage
    {
        public Guid id;

        public SpriteType spriteType;

        public SpriteRenderer renderer;

        public int gridIndexX = -1;

        public int gridIndexY = -1;

        public int sorting;

        public Transform Trans => renderer.transform;

        public bool IsGridIndexValid()
        {
            return gridIndexX >= 0 && gridIndexY >= 0;
        }

        public void Dispose()
        {
            id = Guid.Empty;
            // TODO: This can be improved by using object pool.
            GameObject.Destroy(renderer.gameObject);
            renderer = null;
            gridIndexX = -1;
            gridIndexY = -1;
            sorting = -1;
        }
    }

    [SerializeField]
    private GameObject topLeftMarker;
    [SerializeField]
    private GameObject bottomRightMarker;

    [SerializeField]
    private Transform bgParent;

    [SerializeField]
    private Transform spritesParent;

    [SerializeField]
    private SpriteRenderer rendererPrefab;

    [SerializeField]
    private Sprite[] spritePrefabs;

    private List<Guid> bgTileList = new List<Guid>();

    private Dictionary<Guid, SpriteUsage> spriteUsageLookUp = new Dictionary<Guid, SpriteUsage>();

    private bool isInit;

    public bool IsInit => isInit;

    private int gridSizeX = -1;

    private int gridSizeY = -1;

    private void Start()
    {
        Debug.Assert(topLeftMarker != null, "GridSpriteManager: topLeftMarker cannot be null.");
        Debug.Assert(bottomRightMarker != null, "GridSpriteManager: bottomRightMarker cannot be null.");
        Debug.Assert(rendererPrefab != null, "GridSpriteManager: rendererPrefab cannot be null.");

        isInit = true;
    }

    public void InitBGTileSprites(int sizeX, int sizeY)
    {
        if (bgTileList.Count > 0)
        {
            Debug.LogError("GridSpriteManager: BGTiles already init.");
            return;
        }

        gridSizeX = sizeX;
        gridSizeY = sizeY;

        for (var y = 0; y < sizeY; y++)
        {
            for (var x = 0; x < sizeX; x++)
            {
                var id = CreateSpriteUsage(SpriteType.BGTile, x, y, "BGTile[" + x + "][" + y + "]", bgParent);
                bgTileList.Add(id);
            }
        }
    }

    private Sprite GetSprite(SpriteType spriteType)
    {
        if (spritePrefabs == null)
        {
            Debug.LogError("GridSpriteManager: spritePrefabs is null.");
            return null;
        }

        var index = (int)spriteType - 1;
        if (index < 0 || index >= spritePrefabs.Length)
        {
            Debug.LogError("GridSpriteManager: index " + index + " is out of range.");
            return null;
        }

        return spritePrefabs[index];
    }

    public Guid CreateSpriteUsage(SpriteType spriteType, int gridIndexX, int gridIndexY, string name, Transform overrideParent = null)
    {
        var parent = spritesParent;

        var spritePrefab = GetSprite(spriteType);
        if (overrideParent != null)
            parent = overrideParent;

        var renderer = GameObject.Instantiate<SpriteRenderer>(rendererPrefab, parent);
        renderer.gameObject.name = name;

        var spriteUsage = new SpriteUsage 
        {
            id = Guid.NewGuid(), 
            spriteType = spriteType,
            renderer = renderer, 
            gridIndexX = gridIndexX, 
            gridIndexY = gridIndexY, 
            sorting = GetSortingOrder(spriteType) 
        };

        ApplyPosSpriteAndSorting(spriteUsage, true, 0);

        spriteUsageLookUp.Add(spriteUsage.id, spriteUsage);
        return spriteUsage.id;
    }

    public void RefreshSpriteUsage(Guid id, int gridIndexX, int gridIndexY, SpriteType spriteType = SpriteType.None, float rotZ = 0)
    {
        var succeed = spriteUsageLookUp.TryGetValue(id, out SpriteUsage spriteUsage);
        if (!succeed)
        {
            Debug.LogError("GridSpriteManager: fail to UpdateSpriteUsage(" + id + "). It does not exist.");
            return;
        }

        var applySpriteAndSorting = false;

        spriteUsage.gridIndexX = gridIndexX;
        spriteUsage.gridIndexY = gridIndexY;
        if (spriteType != SpriteType.None && spriteType != spriteUsage.spriteType)
        {
            spriteUsage.spriteType = spriteType;
            spriteUsage.sorting = GetSortingOrder(spriteType);
            applySpriteAndSorting = true;
        }

        ApplyPosSpriteAndSorting(spriteUsage, applySpriteAndSorting, rotZ);
    }

    public void RemoveSpriteUsage(Guid id)
    {
        var succeed = spriteUsageLookUp.TryGetValue(id, out SpriteUsage spriteUsage);
        if (!succeed)
        {
            Debug.LogError("GridSpriteManager: fail to RemoveSpriteUsage(" + id + "). It does not exist.");
            return;
        }

        spriteUsageLookUp.Remove(id);
        spriteUsage.Dispose();
    }

    private int GetSortingOrder(SpriteType spriteType)
    {
        switch (spriteType)
        {
            case SpriteType.BGTile:
                return 0;

            case SpriteType.Bullet:
                return 2;

            default:
                return 1;
        }
    }

    private void ApplyPosSpriteAndSorting(SpriteUsage spriteUsage, bool applySpriteAndSorting, float rotZ)
    {
        var trans = spriteUsage.Trans;

        GetGridPosAndScale(spriteUsage.gridIndexX, spriteUsage.gridIndexY, 
            out float posX, out float posY, out float scaleX, out float scaleY);

        var pos = trans.position;
        var scale = trans.localScale;
        var euler = trans.eulerAngles;

        pos.x = posX;
        pos.y = posY;
        scale.x = scaleX;
        scale.y = scaleY;
        euler.z = rotZ;

        trans.position = pos;
        trans.localScale = scale;
        trans.eulerAngles = euler;

        SetRendererActive(spriteUsage.renderer, spriteUsage.IsGridIndexValid());
        
        if (!applySpriteAndSorting)
            return;

        var sprite = GetSprite(spriteUsage.spriteType);
        spriteUsage.renderer.sprite = sprite;

        spriteUsage.renderer.sortingOrder = spriteUsage.sorting;
    }

    private void SetRendererActive(SpriteRenderer renderer, bool isActive)
    {
        if (renderer.gameObject.activeSelf != isActive)
            renderer.gameObject.SetActive(isActive);
    }

    private void GetGridPosAndScale(int gridIndexX, int gridIndexY, out float posX, out float posY, out float scaleX, out float scaleY)
    {
        var tfPos = topLeftMarker.transform.position;
        var brPos = bottomRightMarker.transform.position;
        var blPos = new Vector2(tfPos.x, brPos.y);

        var distanceX = brPos.x - tfPos.x;
        var distanceY = tfPos.y - brPos.y;

        Debug.Assert(distanceX > 0, "GridSpriteManager: distanceX cannot be zero or negative.");
        Debug.Assert(distanceY > 0, "GridSpriteManager: distanceY cannot be zero or negative.");
        Debug.Assert(gridSizeX > 0, "GridSpriteManager: gridSizeX must ");

        var cellSizeX = distanceX / gridSizeX;
        var cellSizeY = distanceY / gridSizeY;

        var offsetPosX = (gridIndexX * cellSizeX) + (cellSizeX / 2f);
        var offsetPosY = (gridIndexY * cellSizeY) + (cellSizeY / 2f);

        posX = blPos.x + offsetPosX;
        posY = blPos.y + offsetPosY;

        scaleX = cellSizeX;
        scaleY = cellSizeY;
    }
}
