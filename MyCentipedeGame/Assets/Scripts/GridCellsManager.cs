using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCellsManager
{
    private GameplayManager manager;

    private bool isInit;

    public CellEntityBase[,] cellInfos;

    public GridCellsManager(GameplayManager manager)
    {
        this.manager = manager;
    }

    public void Init()
    {
        if (isInit)
        {
            Debug.LogError("GridInfos: already init!");
            return;
        }

        cellInfos = new CellEntityBase[manager.GridSizeX, manager.GridSizeY];

        isInit = true;
    }

    public bool TryFindPlayerSpawnIndex(out int indexX, out int indexY)
    {
        indexX = -1;
        indexY = -1;

        var rowCount = manager.MaxRowCountForPlayerMovement;
        if (rowCount == 0)
        {
            Debug.LogError("GridInfos: MaxRowCountForPlayerMovement is zero. There is no way to find player Spawn pos.");
            return false;
        }

        for (var y = 0; y < rowCount; y++)
        {
            var middleIndex = (int)(manager.GridSizeX / 2f);
            int offset = 0;

            while (middleIndex - offset >= 0 || middleIndex + offset < manager.GridSizeX)
            {
                var lowerIndex = middleIndex - offset;
                if (lowerIndex >= 0 && cellInfos[lowerIndex, y] == null)
                {
                    indexX = lowerIndex;
                    indexY = y;
                    return true;
                }

                var higherIndex = middleIndex + offset;
                if (higherIndex < manager.GridSizeX && cellInfos[higherIndex, y] == null)
                {
                    indexX = higherIndex;
                    indexY = y;
                    return true;
                }

                offset++;
            }
        }

        return false;
    }

    public bool TryFindSpiderSpawnIndex(out int indexX, out int indexY)
    {
        indexX = -1;
        indexY = -1;

        var middleIndexX = (int)(manager.GridSizeX / 2f);
        var middleIndexY = (int)(manager.GridSizeY / 2f);

        var entity = manager.GridCells.GetEntityAtIndex(middleIndexX, middleIndexY);
        if (entity == null)
        {
            indexX = middleIndexX;
            indexY = middleIndexY;
            return true;
        }

        entity = manager.GridCells.GetEntityAtIndex(middleIndexX - 1, middleIndexY);
        if (entity == null)
        {
            indexX = middleIndexX - 1;
            indexY = middleIndexY;
            return true;
        }

        entity = manager.GridCells.GetEntityAtIndex(middleIndexX + 1, middleIndexY);
        if (entity == null)
        {
            indexX = middleIndexX + 1;
            indexY = middleIndexY;
            return true;
        }

        entity = manager.GridCells.GetEntityAtIndex(middleIndexX, middleIndexY + 1);
        if (entity == null)
        {
            indexX = middleIndexX;
            indexY = middleIndexY + 1;
            return true;
        }

        entity = manager.GridCells.GetEntityAtIndex(middleIndexX, middleIndexY - 1);
        if (entity == null)
        {
            indexX = middleIndexX;
            indexY = middleIndexY - 1;
            return true;
        }

        return false;
    }

    public void SetEntityAtIndex(CellEntityBase cellBase, int targetX, int targetY, bool alsoSetGridIndexForCell = true)
    {
        RemoveEntity(cellBase);

        if (alsoSetGridIndexForCell)
            cellBase.SetGridIndex(targetX, targetY);

        cellInfos[targetX, targetY] = cellBase;
    }

    public void RemoveEntity(CellEntityBase cellBase)
    {
        if (!cellBase.IsGridIndexValid())
            return;

        Debug.Assert(cellInfos[cellBase.GridIndexX, cellBase.GridIndexY] == cellBase, 
            "GridCellsManager: RemoveEntity(" + cellBase + ") does not match with value in the cell, X:" + 
            cellBase.GridIndexX + " Y:" + cellBase.GridIndexY);
        cellInfos[cellBase.GridIndexX, cellBase.GridIndexY] = null;

        cellBase.SetGridIndex(-1, -1);
    }

    public CellEntityBase GetEntityAtIndex(int indexX, int indexY)
    {
        if (indexX < 0 || indexX >= manager.GridSizeX ||
            indexY < 0 || indexY >= manager.GridSizeY)
            return null;

        return cellInfos[indexX, indexY];
    }
}
