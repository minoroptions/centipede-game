using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameplayManager : MonoBehaviour
{
    public const int minGridSizeX = 15;

    public const int maxGridSizeY = 20;

    #region Adjustable game Settings

    [Range(minGridSizeX, maxGridSizeY)]
    [SerializeField]
    private int gridSize = minGridSizeX;

    private int gridSizeX;
    private int gridSizeY;

    [SerializeField]
    private float playerPercentScreenHeightLimit = 15;

    [SerializeField]
    private int playerMoveSpeedCellPerSec = 3;
    [SerializeField]
    private int playerBulletSpeedCellPerSec = 10;
    [SerializeField]
    private int playerShootPerSecond = 2;
    [SerializeField]
    private int centipedeMoveSpeedCellPerSec = 4;
    [SerializeField]
    private int spiderMoveSpeedCellPerSec = 2;
    [SerializeField]
    private int playerHp = 3;
    [SerializeField]
    private int mushRoomHp = 3;
    [SerializeField]
    private float spiderKillPlayerOverSec = 0.5f;

    private int mushRoomSpawnPerRow = 1;

    [SerializeField]
    private TextMeshPro liveAndScoreText;
    [SerializeField]
    private TextMeshPro gameOverText;

    #endregion

    #region Properties

    public int GridSizeX => gridSizeX;
    public int GridSizeY => gridSizeY;
    public float PlayerPercentScreenHeightLimit => playerPercentScreenHeightLimit;
    public int PlayerMoveSpeedCellPerSec => playerMoveSpeedCellPerSec;
    public int PlayerBulletSpeedCellPerSec => playerBulletSpeedCellPerSec;
    public int PlayerShootPerSecond => playerShootPerSecond;
    public int CentipedeMoveSpeedCellPerSec => centipedeMoveSpeedCellPerSec;
    public int SpiderMoveSpeedCellPerSec => spiderMoveSpeedCellPerSec;
    public int PlayerHp => playerHp;
    public int MushRoomHp => mushRoomHp;
    public int MushRoomSpawnPerRow => mushRoomSpawnPerRow;
    public float SpiderKillPlayerOverSec => spiderKillPlayerOverSec;

    public int MaxRowCountForPlayerMovement
    {
        get { return (int)(gridSizeY * (playerPercentScreenHeightLimit * 0.01f)); }
    }

    #endregion

    #region Managers

    private GridSpriteManager gridSprites;
    public GridSpriteManager GridSprites => gridSprites;

    private GridCellsManager gridCells;
    public GridCellsManager GridCells => gridCells;

    private GameEntitiesManager gameEntities;
    public GameEntitiesManager GameEntities => gameEntities;

    #endregion

    private bool isGameOver;
    public bool IsGameOver => isGameOver;

    private void Start()
    {
        var meshRenderer = liveAndScoreText.GetComponent<MeshRenderer>();
        meshRenderer.sortingOrder = 10;
        meshRenderer = gameOverText.GetComponent<MeshRenderer>();
        meshRenderer.sortingOrder = 10;

        gridSizeX = gridSize;
        gridSizeY = gridSize;

        gridSprites = FindObjectOfType<GridSpriteManager>();
        if (gridSprites == null)
        {
            Debug.LogError("GameplayManager: gridSprites cannot be null. It is missing from the scene.");
            return;
        }

        Debug.Assert(gridSprites.IsInit, "GameplayManager: gridManager.IsInit has to be true at this point.");
        gridSprites.InitBGTileSprites(gridSizeX, gridSizeY);

        gridCells = new GridCellsManager(this);
        gridCells.Init();

        gameEntities = new GameEntitiesManager(this);
        StartNewGame();
    }

    public bool IsIndexOutOfGridBound(int indexX, int indexY)
    {
        if (indexX < 0 || indexX >= gridSizeX ||
            indexY < 0 || indexY >= gridSizeY)
            return true; // out of bound.

        return false;
    }

    public void RefreshLiveAndScoreText()
    {
        var text = string.Format("LIVE: {0}, SCORE: {1}", gameEntities.PlayerHp, gameEntities.Score);

        liveAndScoreText.text = text;
    }

    #region Start, End, and GameOver

    private void StartNewGame()
    {
        isGameOver = false;

        gameOverText.gameObject.SetActive(false);

        gameEntities.StartNewGame();

        RefreshLiveAndScoreText();
    }

    private void EndCurrentGame()
    {
        gameEntities.EndCurrentGame();
    }

    public void GameOver()
    {
        isGameOver = true;
        gameOverText.gameObject.SetActive(true);
    }

    #endregion

    #region Coroutine Methods

    public Coroutine StartActionDelay(float speedPerSec, UnityAction callback)
    {
        if (speedPerSec <= 0)
        {
            Debug.LogError("GameplayManager: speedPerSec is zero or less");
            return null;
        }

        var delaySec = 1f / speedPerSec;

        return StartDelaySec(delaySec, callback);
    }

    public Coroutine StartDelaySec(float delaySec, UnityAction callback)
    {
        var routine = StartCoroutine(DelayRoutine(delaySec, callback));
        return routine;
    }

    private IEnumerator DelayRoutine(float waitForSec, UnityAction callbackWhenFinished)
    {
        if (waitForSec > 0)
            yield return new WaitForSeconds(waitForSec);

        if (isGameOver)
            yield break;

        callbackWhenFinished?.Invoke();
    }

    #endregion

    // Update is called once per frame
    void Update()
    {
        if (isGameOver && Input.GetKeyDown(KeyCode.Space))
        {
            // end and restart a new game.
            EndCurrentGame();
            StartNewGame();
        }

        gameEntities?.Update();
    }
}
