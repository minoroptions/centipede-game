using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HittableEntityBase : CellEntityBase
{
    public HittableEntityBase(GameplayManager manager) : base(manager)
    {

    }

    public abstract void OnBulletHit();
}
