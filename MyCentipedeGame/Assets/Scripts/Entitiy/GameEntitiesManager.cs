using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEntitiesManager
{
    private GameplayManager manager;

    private PlayerEntity player;
    public PlayerEntity Player => player;

    private HashSet<CellEntityBase> tempSet = new HashSet<CellEntityBase>();


    private HashSet<BulletEntity> bullets = new HashSet<BulletEntity>();

    private HashSet<CenHeadEntity> centipedes = new HashSet<CenHeadEntity>();

    private HashSet<MushEntity> mushrooms = new HashSet<MushEntity>();

    private SpiderEntity spider;

    private Coroutine playerRespawnRoutine;

    private Coroutine centipedeMoveRoutine;

    private Coroutine spiderRespawnRoutine;


    private int playerHp;
    public int PlayerHp => playerHp;

    private int score;
    public int Score => score;

    public bool IsCentipedeAllowToMove
    {
        get { return centipedeMoveRoutine == null; }
    }

    public GameEntitiesManager(GameplayManager manager)
    {
        this.manager = manager;
    }

    #region Required for Spider

    public void OnPlayerMoved()
    {
        spider?.OnPlayerMoved();
    }

    private void SpiderDelayCallback()
    {
        spiderRespawnRoutine = null;

        if (spider.IsGridIndexValid())
            return;

        var succeed = spider.Spawn();
        if (succeed)
            return;

        Debug.LogWarning("FAIL to spawn spider");
        spiderRespawnRoutine = manager.StartDelaySec(1, SpiderDelayCallback);
    }

    #endregion

    #region Kill Methods

    public void KillPlayer(CellEntityBase entity)
    {
        if (playerRespawnRoutine != null)
        {
            Debug.LogError("GameEntitiesManager: respawnPlayerRoutine is running, how can player be killed again?");
            return;
        }

        player.Despawn();

        playerHp -= 1;
        manager.RefreshLiveAndScoreText();

        if (playerHp == 0)
        {
            manager.GameOver();
            return;
        }

        // reduce life and respawn.
        playerRespawnRoutine = manager.StartDelaySec(1.5f, () =>
        {
            playerRespawnRoutine = null;
            player.Spawn();
        });
    }

    public void KillSpider(bool addScore = true)
    {
        if (spiderRespawnRoutine != null)
        {
            //Debug.LogError("GameEntitiesManager: spiderRespawnRoutine is running, how can spider be killed again?");
            if (spider.IsGridIndexValid())
                spider.Despawn();

            return;
        }

        if (addScore)
        {
            score += 100;
            manager.RefreshLiveAndScoreText();
        }

        spider.Despawn();
        spiderRespawnRoutine = manager.StartDelaySec(10, SpiderDelayCallback);
    }

    #endregion

    #region Destroy Methods

    public void DestroyBullet(BulletEntity bullet)
    {
        bullets.Remove(bullet);
        bullet.Dispose();
    }

    public void DestroyCenHead(CenHeadEntity cenHead)
    {
        var succeed = centipedes.Remove(cenHead);
        Debug.Assert(succeed, "GameEntitiesManager: fail remove cenHead from the HashSet in Destroy().");

        cenHead.Dispose();

        score += 300;
        manager.RefreshLiveAndScoreText();
    }

    public void DestroyMushroom(MushEntity mush, bool addScore = true)
    {
        var succeed = mushrooms.Remove(mush);
        Debug.Assert(succeed, "GameEntitiesManager: fail remove mushroom from the HashSet in Destroy().");

        mush.Dispose();

        if (!addScore)
            return;

        score += 100;
        manager.RefreshLiveAndScoreText();
    }

    #endregion

    #region Required for Centipede

    public void OnBulletHitCenBody()
    {
        score += 100;
        manager.RefreshLiveAndScoreText();
    }

    public void CheckCenHeadLeftForGameOver()
    {
        if (centipedes.Count > 0)
            return;

        manager.GameOver();
    }

    #endregion

    #region Start and End Game Methods

    public void StartNewGame()
    {
        playerHp = manager.PlayerHp;
        score = 0;
        manager.RefreshLiveAndScoreText();

        SpawnPlayer();

        SpawnCenHeadStartGame();

        SpawnMushroomStartGame();

        SpawnSpider();
    }

    public void EndCurrentGame()
    {
        if (player != null)
        {
            player.Dispose();
            player = null;
        }

        foreach (var bullet in bullets)
            bullet.Dispose();
        bullets.Clear();

        foreach (var cen in centipedes)
            cen.Dispose();
        centipedes.Clear();

        foreach (var mush in mushrooms)
            mush.Dispose();
        mushrooms.Clear();

        if (spider != null)
        {
            spider.Dispose();
            spider = null;
        }

        StopCoroutine(ref playerRespawnRoutine);
        StopCoroutine(ref centipedeMoveRoutine);
        StopCoroutine(ref spiderRespawnRoutine);
    }

    private void StopCoroutine(ref Coroutine coroutine)
    {
        if (coroutine != null)
        {
            manager.StopCoroutine(coroutine);
            coroutine = null;
        }
    }

    #endregion

    #region Spawn Methods

    #region Spawn for Start Game

    public void SpawnCenHeadStartGame()
    {
        List<GridPos> cenPosList = new List<GridPos>();

        for (var i = 14; i >= 0; i--)
            cenPosList.Add(new GridPos { x = i, y = manager.GridSizeY - 1 });

        SpawnCenHead(cenPosList, CenHeadEntity.Direction.Left, false);
    }

    public void SpawnMushroomStartGame()
    {
        var gridSizeY = manager.GridSizeY;
        var mushPerRow = manager.MushRoomSpawnPerRow;

        for (var y = 1; y < gridSizeY - 2; y++)
        {
            for (var m = 0; m < mushPerRow; m++)
            {
                var succeed = TryGetXToSpawnMush(y, out int x);
                if (!succeed)
                    continue;

                SpawnMushroom(x, y);
            }
        }
    }

    private bool TryGetXToSpawnMush(int y, out int x)
    {
        var gridSizeX = manager.GridSizeX;

        // try at maximum 3 times.
        for (var i = 0; i < 3; i++)
        {
            x = Random.Range(0, gridSizeX);
            var entity = manager.GridCells.GetEntityAtIndex(x, y);
            if (entity != null)
                continue;

            return true;
        }

        x = -1;
        return false;
    }

    #endregion

    public void SpawnPlayer()
    {
        if (player == null)
            player = new PlayerEntity(manager);

        player.Spawn();
    }

    public void SpawnCenHead(List<GridPos> cenPosList, CenHeadEntity.Direction headDirection, bool isUpWhenCollide)
    {
        var cenHead = new CenHeadEntity(manager);
        cenHead.Spawn(cenPosList, headDirection, isUpWhenCollide);

        centipedes.Add(cenHead);
    }

    public void SpawnSpider()
    {
        if (spider == null)
            spider = new SpiderEntity(manager);

        spider.Spawn();
    }

    public void SpawnMushroom(int x, int y)
    {
        var mush = new MushEntity(manager);
        mush.Spawn(x, y);

        mushrooms.Add(mush);
    }
    public void SpawnBullet(PlayerEntity player)
    {
        if (!player.IsGridIndexValid())
            return;

        var bullet = new BulletEntity(manager);
        bullets.Add(bullet);

        bullet.SpawnFromPlayer(player);
    }

    #endregion

    #region Updates

    public void Update()
    {
        if (manager.IsGameOver)
            return;

        player?.Update();

        UpdateBullets();

        UpdateCenHeads();

        UpdateSpider();
    }

    private void UpdateBullets()
    {
        if (bullets.Count == 0)
            return;

        Debug.Assert(tempSet.Count == 0);

        foreach (var value in bullets)
            tempSet.Add(value);

        foreach (var value in tempSet)
            value.Update();

        tempSet.Clear();
    }

    private void UpdateCenHeads()
    {
        if (centipedes.Count == 0)
            return;

        Debug.Assert(tempSet.Count == 0);

        foreach (var value in centipedes)
            tempSet.Add(value);

        foreach (var value in tempSet)
            value.Update();

        tempSet.Clear();

        if (centipedeMoveRoutine == null)
            centipedeMoveRoutine = manager.StartActionDelay(manager.CentipedeMoveSpeedCellPerSec, () =>
            {
                centipedeMoveRoutine = null;
            });
    }

    private void UpdateSpider()
    {
        if (spider == null)
            return;

        spider.Update();
    }

    #endregion
}
