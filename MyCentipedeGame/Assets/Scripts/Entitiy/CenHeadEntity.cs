using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GridPos
{
    public int x;

    public int y;

    public GridPos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public bool Equals(GridPos compare)
    {
        return x == compare.x && y == compare.y;
    }
}

public class CenHeadEntity : HittableEntityBase
{
    public enum Direction
    {
        Right = 0,
        Up = 90,
        Left = 180,
        Down = 270,
    }

    public Direction direction = Direction.Right;

    public bool isUpWhenCollide = false;

    public Direction lastKnownMoveLeftOrRight;

    // the body must be ordered from the one clostest to the head to tell.
    public List<CenBodyEntity> bodyList = new List<CenBodyEntity>();

    public CenHeadEntity(GameplayManager manager) : base(manager)
    {
        spriteId = manager.GridSprites.CreateSpriteUsage(SpriteType.CenHead, gridIndexX, gridIndexY, "CenHead");
    }

    public override bool IsEnemy => true;

    public override void Dispose()
    {
        DestroyCenBody(0);

        Debug.Assert(bodyList.Count == 0);

        base.Dispose();
    }

    public void Spawn(List<GridPos> posList, Direction headDir, bool isUpWhenCollide)
    {
        // [right: rot 0] [up: rot 90] [left: rot 180] [down: rot 270]

        Debug.Assert(bodyList.Count == 0);
        Debug.Assert(posList.Count > 0);

        var headPos = posList[0];
        var isOutOfBound = manager.IsIndexOutOfGridBound(headPos.x, headPos.y);
        var entityAtIndex = manager.GridCells.GetEntityAtIndex(headPos.x, headPos.y);
        if (isOutOfBound)
        {
            Debug.LogError("CenHeadEntity: pos out of bound for the head, or there is an entity at the index when spawning the head.");
            return;
        }

        direction = headDir;
        this.isUpWhenCollide = isUpWhenCollide;

        manager.GridCells.SetEntityAtIndex(this, headPos.x, headPos.y);
        manager.GridSprites.RefreshSpriteUsage(spriteId, headPos.x, headPos.y, SpriteType.None, (int)headDir);

        for (var i = 1; i < posList.Count; i++)
        {
            var bodyPos = posList[i];

            isOutOfBound = manager.IsIndexOutOfGridBound(bodyPos.x, bodyPos.y);
            entityAtIndex = manager.GridCells.GetEntityAtIndex(bodyPos.x, bodyPos.y);
            if (isOutOfBound || entityAtIndex != null)
            {
                Debug.LogError("CenHeadEntity: body pos is out of bound or, there is an entity at the index when spawning the body.");
                return;
            }

            var cenBody = SpawnCenBodyEntity(i - 1);
            bodyList.Add(cenBody);
            Debug.Assert(bodyList.Count - 1 == cenBody.BodyIndex);

            cenBody.MoveBodyTo(bodyPos.x, bodyPos.y);
        }
    }

    private CenBodyEntity SpawnCenBodyEntity(int bodyIndex)
    {
        var cenBody = new CenBodyEntity(manager, this, bodyIndex);
        return cenBody;
    }

    public Direction GetDirectionToUpperBody(int bodyIndex)
    {
        var cenBody = bodyList[bodyIndex];

        if (bodyIndex == 0)
            return GetDirectionToPos(cenBody.GridIndexX, cenBody.GridIndexY, gridIndexX, gridIndexY);

        var upperCenBody = bodyList[bodyIndex - 1];

        return GetDirectionToPos(cenBody.GridIndexX, cenBody.GridIndexY, upperCenBody.GridIndexX, upperCenBody.GridIndexY);
    }

    public Direction GetInvertDirectionToUpperBody(int bodyIndex)
    {
        var dir = GetDirectionToUpperBody(bodyIndex);

        switch (dir)
        {
            case Direction.Right:
                return Direction.Left;

            case Direction.Left:
                return Direction.Right;

            case Direction.Down:
                return Direction.Up;

            case Direction.Up:
                return Direction.Down;
        }

        return Direction.Right;
    }

    private Direction GetDirectionToPos(int fromX, int fromY, int toX, int toY)
    {
        if (fromX > toX)
            return Direction.Left;

        if (fromX < toX)
            return Direction.Right;

        if (fromY > toY)
            return Direction.Down;

        // fromY < toY
        return Direction.Up;
    }

    public override void OnBulletHit()
    {
        // (1) case shoot head, this entity died, the rest of body turns into the new cen.
        var gameEntities = manager.GameEntities;

        if (bodyList.Count == 0)
        {
            gameEntities.DestroyCenHead(this);
            gameEntities.CheckCenHeadLeftForGameOver();
            return;
        }

        var lastIndex = bodyList.Count - 1;
        var newHeadDirection = GetInvertDirectionToUpperBody(lastIndex);
        var upWhenCollide = isUpWhenCollide;
        var posList = new List<GridPos>();

        // prepare to spawn a separate cenHead entity.
        for (var i = lastIndex; i >= 0; i--)
        {
            var cenBody = bodyList[i];
            var pos = new GridPos { x = cenBody.GridIndexX, y = cenBody.GridIndexY };
            posList.Add(pos);
        }

        gameEntities.DestroyCenHead(this);
        gameEntities.SpawnCenHead(posList, newHeadDirection, upWhenCollide);
    }

    public void OnBodyBulletHit(int bodyIndex)
    {
        // (2) case shoot tail tip, only that tip is destroy and the rest of this entity continue.
        var lastIndex = bodyList.Count - 1;
        if (bodyIndex == lastIndex)
        {
            var cenBody = bodyList[lastIndex];
            bodyList.RemoveAt(lastIndex);
            cenBody.Dispose();
            return;
        }

        // (3) case shoot in middle of the body, that cell destroyed, the other part turns into the new cen.
        var newHeadDirection = GetInvertDirectionToUpperBody(bodyList.Count - 1);
        var upWhenCollide = isUpWhenCollide;
        var posList = new List<GridPos>();

        // prepare to spawn a separate cenHead entity.
        for (var i = lastIndex; i > bodyIndex; i--)
        {
            var cenBody = bodyList[i];
            var pos = new GridPos { x = cenBody.GridIndexX, y = cenBody.GridIndexY };
            posList.Add(pos);
        }

        DestroyCenBody(bodyIndex);
        manager.GameEntities.SpawnCenHead(posList, newHeadDirection, upWhenCollide);

        manager.GameEntities.OnBulletHitCenBody();
    }

    private void DestroyCenBody(int startBodyIndex)
    {
        while (startBodyIndex < bodyList.Count)
        {
            var cenBody = bodyList[startBodyIndex];
            cenBody.Dispose();
            bodyList.RemoveAt(startBodyIndex);
        }
    }

    public override void Update()
    {
        if (!manager.GameEntities.IsCentipedeAllowToMove)
            return;

        ProcessMoveNext();
    }

    private void ProcessMoveNext()
    {
        switch (direction)
        {
            case Direction.Right:
                ProcessMoveRight();
                return;

            case Direction.Left:
                ProcessMoveLeft();
                return;

            case Direction.Up:
            case Direction.Down:
                ProcessChangeToMoveLeftOrRight(direction);
                return;
        }
    }

    private void ProcessChangeToMoveUpOrDown()
    {
        var possible = false;

        // if it is not possible, need to try to move up or down.
        if (isUpWhenCollide)
        {
            possible = TryToMoveUp();
            if (possible)
                return;

            isUpWhenCollide = false;
            possible = TryToMoveDown();
            if (possible)
                return;

            return;
        }

        possible = TryToMoveDown();
        if (possible)
            return;

        isUpWhenCollide = true;
        possible = TryToMoveUp();
        if (possible)
            return;

        //Debug.LogWarning("CenHeadEntity: cannot find any suitable direction to move.");
    }

    private void ProcessMoveRight()
    {
        // can we still continue moving right?.
        var possible = TryToMoveRight();
        if (possible)
            return;

        ProcessChangeToMoveUpOrDown();
    }

    private void ProcessMoveLeft()
    {
        // can we still continue moving left?.
        var possible = TryToMoveLeft();
        if (possible)
            return;

        ProcessChangeToMoveUpOrDown();
    }

    private void ProcessChangeToMoveLeftOrRight(Direction currentDir)
    {
        var possible = false;

        if (lastKnownMoveLeftOrRight == Direction.Left)
        {
            // go right.
            possible = TryToMoveRight();
            if (possible)
                return;

            possible = TryToMoveLeft();
            if (possible)
                return;

            if (currentDir == Direction.Up)
            {
                possible = TryToMoveUp();
                if (possible)
                    return;
            }

            if (currentDir == Direction.Down)
            {
                possible = TryToMoveDown();
                if (possible)
                    return;
            }

            return;
        }

        possible = TryToMoveLeft();
        if (possible)
            return;

        possible = TryToMoveRight();
        if (possible)
            return;

        if (currentDir == Direction.Up)
        {
            possible = TryToMoveUp();
            if (possible)
                return;
        }

        if (currentDir == Direction.Down)
        {
            possible = TryToMoveDown();
            if (possible)
                return;
        }

        //Debug.LogWarning("CenHeadEntity: cannot find any suitable direction to move left or right.");
    }

    private void MoveTo(GridPos pos, Direction toMoveDir, CellEntityBase collidedEntity)
    {
        var bulletHitHeadAfterMove = false;

        if (collidedEntity is PlayerEntity player)
        {
            // kill the player before move.
            manager.GameEntities.KillPlayer(this);
        }
        else if (collidedEntity is BulletEntity bullet)
        {
            manager.GameEntities.DestroyBullet(bullet);
            bulletHitHeadAfterMove = true;
        }

        var toMoveBodyPos = new GridPos { x = gridIndexX, y = gridIndexY };

        direction = toMoveDir;
        if (direction == Direction.Right || direction == Direction.Left)
            lastKnownMoveLeftOrRight = direction;

        manager.GridCells.SetEntityAtIndex(this, pos.x, pos.y);
        manager.GridSprites.RefreshSpriteUsage(spriteId, pos.x, pos.y, SpriteType.None, (int)direction);

        for (var i = 0; i < bodyList.Count; i++)
        {
            var cenBody = bodyList[i];

            var cachedPos = new GridPos { x = cenBody.GridIndexX, y = cenBody.GridIndexY };
            cenBody.MoveBodyTo(toMoveBodyPos.x, toMoveBodyPos.y);

            toMoveBodyPos = cachedPos;
        }

        if (bulletHitHeadAfterMove)
        {
            OnBulletHit();
            return;
        }
    }

    private bool TryToMoveRight()
    {
        var pos = new GridPos { x = gridIndexX + 1, y = gridIndexY };
        var possible = TryToCheckMoveTo(pos, out CellEntityBase collidedEntity);
        if (possible)
            MoveTo(pos, Direction.Right, collidedEntity);

        return possible;
    }

    private bool TryToDestroyIfMushOrSpider(CellEntityBase collidedEntity)
    {
        if (collidedEntity is MushEntity mush)
        {
            manager.GameEntities.DestroyMushroom(mush, false);
            return true;
        }

        if (collidedEntity is SpiderEntity)
        {
            manager.GameEntities.KillSpider(false);
            return true;
        }

        return false;
    }

    private bool TryToMoveUp()
    {
        var pos = new GridPos { x = gridIndexX, y = gridIndexY + 1};
        var possible = TryToCheckMoveTo(pos, out CellEntityBase collidedEntity);
        if (possible)
        {
            MoveTo(pos, Direction.Up, collidedEntity);
            return true;
        }

        possible = TryToDestroyIfMushOrSpider(collidedEntity);
        if (possible)
            MoveTo(pos, Direction.Up, null);

        return possible;
    }

    private bool TryToMoveDown()
    {
        var pos = new GridPos { x = gridIndexX, y = gridIndexY - 1 };
        var possible = TryToCheckMoveTo(pos, out CellEntityBase collidedEntity);
        if (possible)
        {
            MoveTo(pos, Direction.Down, collidedEntity);
            return true;
        }

        possible = TryToDestroyIfMushOrSpider(collidedEntity);
        if (possible)
            MoveTo(pos, Direction.Down, null);

        return possible;
    }

    private bool TryToMoveLeft()
    {
        var pos = new GridPos { x = gridIndexX - 1, y = gridIndexY };
        var possible = TryToCheckMoveTo(pos, out CellEntityBase collidedEntity);
        if (possible)
            MoveTo(pos, Direction.Left, collidedEntity);

        return possible;
    }

    private bool TryToCheckMoveTo(GridPos pos, out CellEntityBase collidedEntity)
    {
        collidedEntity = null;

        var isOutOfBound = manager.IsIndexOutOfGridBound(pos.x, pos.y);
        if (isOutOfBound)
            return false;

        collidedEntity = manager.GridCells.GetEntityAtIndex(pos.x, pos.y);
        if (collidedEntity == null)
            return true;

        // centipede can move to collide with player or bullet.
        if (collidedEntity is PlayerEntity || collidedEntity is BulletEntity)
            return true;

        return false;
    }
}
