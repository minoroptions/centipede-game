using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CellEntityBase
{
    protected GameplayManager manager;

    protected int gridIndexX = -1;
    public int GridIndexX => gridIndexX;

    protected int gridIndexY = -1;
    public int GridIndexY => gridIndexY;


    protected Guid spriteId;

    protected Coroutine delayDefaultRoutine;

    public CellEntityBase(GameplayManager manager)
    {
        this.manager = manager;
    }

    public virtual bool IsEnemy => false;

    public virtual void Dispose()
    {
        if (spriteId != Guid.Empty)
        {
            manager.GridSprites.RemoveSpriteUsage(spriteId);
            spriteId = Guid.Empty;
        }

        manager.GridCells.RemoveEntity(this);

        if (delayDefaultRoutine != null)
        {
            manager.StopCoroutine(delayDefaultRoutine);
            delayDefaultRoutine = null;
        }

        manager = null;
    }

    public GridPos GetRightPos()
    {
        if (!IsGridIndexValid())
            return new GridPos { x = -1, y = -1 };

        return new GridPos { x = gridIndexX + 1, y = gridIndexY };
    }

    public GridPos GetLeftPos()
    {
        if (!IsGridIndexValid())
            return new GridPos { x = -1, y = -1 };

        return new GridPos { x = gridIndexX - 1, y = gridIndexY };
    }

    public GridPos GetUpPos()
    {
        if (!IsGridIndexValid())
            return new GridPos { x = -1, y = -1 };

        return new GridPos { x = gridIndexX, y = gridIndexY + 1 };
    }

    public GridPos GetDownPos()
    {
        if (!IsGridIndexValid())
            return new GridPos { x = -1, y = -1 };

        return new GridPos { x = gridIndexX, y = gridIndexY - 1 };
    }

    public bool IsGridIndexValid()
    {
        return !manager.IsIndexOutOfGridBound(gridIndexX, gridIndexY);
    }

    public void SetGridIndex(int x, int y)
    {
        gridIndexX = x;
        gridIndexY = y;
    }

    protected void StartDefaultDelay(float speedPerSec)
    {
        if (delayDefaultRoutine != null)
        {
            Debug.LogError("delayDefaultRoutine must be null.");
            return;
        }

        delayDefaultRoutine = manager.StartActionDelay(speedPerSec, () =>
        {
            Debug.Assert(delayDefaultRoutine != null);
            delayDefaultRoutine = null;
        });
    }

    public abstract void Update();
}
