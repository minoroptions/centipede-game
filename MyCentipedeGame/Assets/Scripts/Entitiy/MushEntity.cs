using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushEntity : HittableEntityBase
{
    private int hp;

    public MushEntity(GameplayManager manager) : base(manager)
    {
        spriteId = manager.GridSprites.CreateSpriteUsage(SpriteType.MushHigh, gridIndexX, gridIndexY, "Mushroom");
        hp = manager.MushRoomHp;
    }

    public void Spawn(int x, int y)
    {
        if (manager.IsIndexOutOfGridBound(x, y))
        {
            Debug.LogError("MushEntity: cannot spawn at pos. X:" + x + " Y:" + y);
            return;
        }

        manager.GridCells.SetEntityAtIndex(this, x, y);
        manager.GridSprites.RefreshSpriteUsage(spriteId, x, y, GetSpriteType());
    }

    public override void OnBulletHit()
    {
        hp -= 1;
        if (hp == 0)
        {
            manager.GameEntities.DestroyMushroom(this);
            return;
        }

        // update sprite.
        manager.GridSprites.RefreshSpriteUsage(spriteId, gridIndexX, gridIndexY, GetSpriteType());
    }

    private SpriteType GetSpriteType()
    {
        float hpF = hp;
        var amountLeft = hpF / manager.MushRoomHp;

        if (amountLeft < 0.4f)
            return SpriteType.MushLow;

        if (amountLeft < 1)
            return SpriteType.MushMed;

        return SpriteType.MushHigh;
    }

    public override void Update()
    {
        // nothing.
    }
}
