using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerEntity : CellEntityBase
{
    protected Coroutine delayShootRoutine;

    //public UnityAction onPosChanged;

    public PlayerEntity(GameplayManager manager) : base(manager)
    {
        spriteId = manager.GridSprites.CreateSpriteUsage(SpriteType.Player, gridIndexX, gridIndexY, "PlayerSprite");
    }

    public override void Dispose()
    {
        if (delayShootRoutine != null)
        {
            manager.StopCoroutine(delayShootRoutine);
            delayShootRoutine = null;
        }

        base.Dispose();
    }

    public void Spawn()
    {
        var succeed = manager.GridCells.TryFindPlayerSpawnIndex(out int indexX, out int indexY);
        if (!succeed)
        {
            Debug.LogError("PlayerCellEntity: fail to spawn player.");
            return;
        }

        Move(indexX, indexY, false);
        // prevent restarting the game and then shoot at the same time.
        StartShootDelay(2);
    }

    public void Despawn()
    {
        manager.GridCells.RemoveEntity(this);

        manager.GridSprites.RefreshSpriteUsage(spriteId, gridIndexX, gridIndexY);
    }

    protected void StartShootDelay(int playerShootPerSecond)
    {
        if (delayShootRoutine != null)
        {
            Debug.LogError("delayShootRoutine must be null.");
            return;
        }

        delayShootRoutine = manager.StartActionDelay(playerShootPerSecond, () =>
        {
            Debug.Assert(delayShootRoutine != null);
            delayShootRoutine = null;
        });
    }

    public override void Update()
    {
        if (!IsGridIndexValid())
            return;

        if (Input.GetKey(KeyCode.UpArrow))
            Move(gridIndexX, gridIndexY + 1);
        else if (Input.GetKey(KeyCode.DownArrow))
            Move(gridIndexX, gridIndexY - 1);
        else if (Input.GetKey(KeyCode.LeftArrow))
            Move(gridIndexX - 1, gridIndexY);
        else if (Input.GetKey(KeyCode.RightArrow))
            Move(gridIndexX + 1, gridIndexY);

        if (Input.GetKey(KeyCode.Space))
            Shoot();
    }

    private void Shoot()
    {
        if (delayShootRoutine != null)
            return; // during shoot delay.

        manager.GameEntities.SpawnBullet(this);

        StartShootDelay(manager.PlayerShootPerSecond);
    }

    private void Move(int targetX, int targetY, bool triggerDelay = true)
    {
        if (delayDefaultRoutine != null)
            return; // during move delay.

        if (manager.IsIndexOutOfGridBound(targetX, targetY))
            return; // out of bound.

        if (manager.MaxRowCountForPlayerMovement < targetY)
            return; // player cannot go further.

        var entity = manager.GridCells.GetEntityAtIndex(targetX, targetY);
        if (entity != null)
        {
            if (entity.IsEnemy)
            {
                manager.GameEntities.KillPlayer(entity);
                return;
            }

            // cannot move over other things.
            return;
        }

        manager.GridCells.SetEntityAtIndex(this, targetX, targetY);
        manager.GridSprites.RefreshSpriteUsage(spriteId, targetX, targetY);

        if (triggerDelay)
            StartDefaultDelay(manager.PlayerMoveSpeedCellPerSec);

        manager.GameEntities.OnPlayerMoved();
    }
}
