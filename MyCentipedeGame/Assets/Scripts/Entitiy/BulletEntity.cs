using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEntity : CellEntityBase
{
    public BulletEntity(GameplayManager manager) : base(manager)
    {
        spriteId = manager.GridSprites.CreateSpriteUsage(SpriteType.Bullet, gridIndexX, gridIndexY, "Bullet");
    }

    public void SpawnFromPlayer(PlayerEntity player)
    {
        Move(player.GridIndexX, player.GridIndexY + 1);
    }

    public override void Dispose()
    {
        base.Dispose();
    }

    private void Move(int x, int y)
    {
        if (manager.IsIndexOutOfGridBound(x, y))
        {
            manager.GameEntities.DestroyBullet(this);
            return;
        }

        var entity = manager.GridCells.GetEntityAtIndex(x, y);
        if (entity != null)
        {
            manager.GameEntities.DestroyBullet(this);

            if (entity is HittableEntityBase hittable)
                hittable.OnBulletHit();

            return;
        }

        manager.GridCells.SetEntityAtIndex(this, x, y);
        manager.GridSprites.RefreshSpriteUsage(spriteId, x, y);

        StartDefaultDelay(manager.PlayerBulletSpeedCellPerSec);
    }

    public override void Update()
    {
        if (delayDefaultRoutine != null)
            return;

        Move(gridIndexX, gridIndexY + 1);
    }
}
