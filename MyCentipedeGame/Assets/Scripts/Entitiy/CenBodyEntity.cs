using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenBodyEntity : HittableEntityBase
{
    protected CenHeadEntity head;

    protected int bodyIndex = -1;
    public int BodyIndex => bodyIndex;

    public CenBodyEntity(GameplayManager manager, CenHeadEntity head, int bodyIndex) : base(manager)
    {
        this.head = head;
        this.bodyIndex = bodyIndex;

        spriteId = manager.GridSprites.CreateSpriteUsage(SpriteType.CenBody, gridIndexX, gridIndexY, "CenBody");
    }

    public override bool IsEnemy => true;

    public override void Dispose()
    {
        bodyIndex = -1;
        head = null;

        base.Dispose();
    }

    public void MoveBodyTo(int x, int y)
    {
        if (manager.IsIndexOutOfGridBound(x, y))
        {
            Debug.LogError("CenBodyEntity: cannot moveBodyTo out of bound pos. X:" + x + " Y:" + y);
            return;
        }

        manager.GridCells.SetEntityAtIndex(this, x, y);

        var direction = head.GetDirectionToUpperBody(bodyIndex);
        manager.GridSprites.RefreshSpriteUsage(spriteId, x, y, SpriteType.None, (int)direction);
    }

    public override void OnBulletHit()
    {
        head.OnBodyBulletHit(bodyIndex);
    }

    public override void Update()
    {
        // update by the head.
    }
}
