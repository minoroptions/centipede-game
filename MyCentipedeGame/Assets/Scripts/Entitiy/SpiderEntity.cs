using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderEntity : HittableEntityBase
{
    private enum Direction
    {
        None,
        Right,
        Left,
        Up,
        Down,
    }

    private struct MovePriority
    {
        public GridPos pos;

        public Direction dir;

        /// <summary>
        /// Low means important.
        /// </summary>
        public int priority;
    }

    private List<MovePriority> tempList = new List<MovePriority>();

    protected float overPlayerDurationCounter;

    private GridPos? lastMovePos;

    #region Shortest Path Variables

    private bool[,] visitied;

    private Queue<int> xQ = new Queue<int>();

    private Queue<int> yQ = new Queue<int>();

    private bool reachedTarget;

    private int nodesLeft = 0;
    private int nodesNext = 0;

    private int[] surroundX = new int[] { 0, 0, 1, -1 };
    private int[] surroundY = new int[] { 1, -1, 0, 0 };

    private GridPos?[,] prevPoses;

    private List<GridPos> movePaths = new List<GridPos>();

    #endregion

    public SpiderEntity(GameplayManager manager) : base(manager)
    {
        spriteId = manager.GridSprites.CreateSpriteUsage(SpriteType.Spider, gridIndexX, gridIndexY, "Spider");

        visitied = new bool[manager.GridSizeX, manager.GridSizeY];
        prevPoses = new GridPos?[manager.GridSizeX, manager.GridSizeY];
    }

    #region Shortest Path Operations

    private void ResetVisitedAndPrevious()
    {
        for (var x = 0; x < manager.GridSizeX; x++)
        {
            for (var y = 0; y < manager.GridSizeY; y++)
            {
                visitied[x, y] = false;
                prevPoses[x, y] = null;
            }
        }
    }

    private int GetLowestYAllowed()
    {
        var player = manager.GameEntities.Player;
        if (player.IsGridIndexValid())
        {
            if (gridIndexY <= player.GridIndexY + 1)
                return gridIndexY;

            return player.GridIndexY + 1;
        }

        return 0;
    }

    private bool TryGetTargetPos(out GridPos targetPos)
    {
        targetPos = new GridPos(-1, -1);

        var player = manager.GameEntities.Player;
        if (!player.IsGridIndexValid())
            return false;

        targetPos = new GridPos(player.GridIndexX, player.GridIndexY + 1);
        return true;
    }

    private void ProcessShortestPath()
    {
        movePaths.Clear();

        var succeed = TryGetTargetPos(out GridPos targetPos);
        if (!succeed)
            return;

        if (targetPos.x == gridIndexX && targetPos.y == gridIndexY)
            return;

        reachedTarget = false;
        nodesLeft = 1;
        nodesNext = 0;

        var lowestY = GetLowestYAllowed();

        ResetVisitedAndPrevious();

        GridPos startPos = new GridPos(gridIndexX, gridIndexY);

        xQ.Enqueue(gridIndexX);
        yQ.Enqueue(gridIndexY);

        visitied[gridIndexX, gridIndexY] = true;
        while (xQ.Count > 0)
        {
            var x = xQ.Dequeue();
            var y = yQ.Dequeue();

            var pos = new GridPos(x, y);
            if (pos.Equals(targetPos))
            {
                reachedTarget = true;
                targetPos = new GridPos(x, y);
                break;
            }

            ProcessSurroundCells(x, y, lowestY);

            nodesLeft--;
            if (nodesLeft == 0)
            {
                nodesLeft = nodesNext;
                nodesNext = 0;
            }
        }

        xQ.Clear();
        yQ.Clear();

        if (reachedTarget)
            ProcessMovePathsList(startPos, targetPos);
    }

    private void ProcessMovePathsList(GridPos startPos, GridPos targetPos)
    {
        var currentPos = targetPos;

        while (!currentPos.Equals(startPos))
        {
            var prevPos = prevPoses[currentPos.x, currentPos.y];
            Debug.Assert(prevPos.HasValue);

            movePaths.Add(prevPos.Value);
            currentPos = prevPos.Value;
        }

        movePaths.Reverse();
        movePaths.Add(targetPos);
    }

    private void ProcessSurroundCells(int x, int y, int lowestY)
    {
        var sizeX = manager.GridSizeX;
        var sizeY = manager.GridSizeY;

        for (var i = 0; i < 4; i++)
        {
            var nX = x + surroundX[i];
            var nY = y + surroundY[i];

            if (nX < 0 || nX >= sizeX || nY < lowestY || nY >= sizeY)
                continue;

            if (visitied[nX, nY])
                continue;

            var entity = manager.GridCells.GetEntityAtIndex(nX, nY);
            if (entity != null && !(entity is PlayerEntity || entity is BulletEntity))
                continue;

            xQ.Enqueue(nX);
            yQ.Enqueue(nY);

            visitied[nX, nY] = true;
            prevPoses[nX, nY] = new GridPos(x, y);

            nodesNext++;
        }
    }

    #endregion

    public override bool IsEnemy => true;

    public void OnPlayerMoved()
    {
        lastMovePos = null;
    }

    public bool Spawn()
    {
        if (IsGridIndexValid())
            return false;

        var succeed = manager.GridCells.TryFindSpiderSpawnIndex(out int x, out int y);
        if (!succeed)
            return false;

        lastMovePos = null;
        overPlayerDurationCounter = 0;

        Move(x, y);

        return true;
    }

    public void Despawn()
    {
        manager.GridCells.RemoveEntity(this);
        manager.GridSprites.RefreshSpriteUsage(spriteId, gridIndexX, gridIndexY);

        if (delayDefaultRoutine != null)
        {
            manager.StopCoroutine(delayDefaultRoutine);
            delayDefaultRoutine = null;
        }
    }

    public override void OnBulletHit()
    {
        manager.GameEntities.KillSpider();
    }

    public override void Update()
    {
        if (!IsGridIndexValid())
            return;

        UpdateMoveToPlayer();

        UpdateKillPlayer();
    }

    protected void UpdateKillPlayer()
    {
        var entity = manager.GridCells.GetEntityAtIndex(gridIndexX, gridIndexY - 1);
        if (!(entity is PlayerEntity))
        {
            overPlayerDurationCounter = 0;
            return;
        }

        overPlayerDurationCounter += Time.deltaTime;
        if (overPlayerDurationCounter < manager.SpiderKillPlayerOverSec)
            return;

        overPlayerDurationCounter = 0;
        manager.GameEntities.KillPlayer(this);
    }

    protected void UpdateMoveToPlayer()
    {
        if (delayDefaultRoutine != null)
            return;

        ProcessShortestPath();

        // index 0 is the currentPos. index 1 is the next shortest move.
        if (movePaths.Count >= 2)
        {
            Move(movePaths[1].x, movePaths[1].y);
            return;
        }
        
        GridPos targetPos;

        var player = manager.GameEntities.Player;
        if (player == null || !player.IsGridIndexValid())
            targetPos = new GridPos { x = (int)(manager.GridSizeX / 2f), y = (int)(manager.GridSizeY / 2f) };
        else
            targetPos = new GridPos { x = player.GridIndexX, y = player.GridIndexY + 1 };

        var distanceX = Mathf.Abs(gridIndexX - targetPos.x);
        var distanceY = Mathf.Abs(gridIndexY - targetPos.y);

        // already at target
        if (distanceX + distanceY == 0)
        {
            lastMovePos = null;
            return;
        } 

        if (!manager.IsIndexOutOfGridBound(targetPos.x, targetPos.y))
        {
            var entity = manager.GridCells.GetEntityAtIndex(targetPos.x, targetPos.y);
            if (entity != null && !(entity is BulletEntity) && distanceX + distanceY <= 1)
            {
                lastMovePos = null;
                return;
            }
        }

        Debug.Assert(tempList.Count == 0);

        #region An easy finding logic

        var defaultPriority = 100;
        int priorityRight = defaultPriority;
        int priorityLeft = defaultPriority;
        int priorityUp = defaultPriority;
        int priorityDown = defaultPriority;

        if (targetPos.x > gridIndexX)
        {
            priorityRight -= distanceX;
            priorityLeft += distanceX;
        }
        else if (targetPos.x < gridIndexX)
        {
            priorityLeft -= distanceX;
            priorityRight += distanceX;
        }

        AddToPriorityTempList(new MovePriority { pos = GetRightPos(), dir = Direction.Right, priority = priorityRight });
        AddToPriorityTempList(new MovePriority { pos = GetLeftPos(), dir = Direction.Left, priority = priorityLeft });

        if (targetPos.y > gridIndexY)
        {
            priorityUp -= distanceY;
            priorityDown += distanceY;
        }
        else if (targetPos.y < gridIndexY)
        {
            priorityDown -= distanceY;
            priorityUp += distanceY;
        }

        AddToPriorityTempList(new MovePriority { pos = GetUpPos(), dir = Direction.Up, priority = priorityUp });
        AddToPriorityTempList(new MovePriority { pos = GetDownPos(), dir = Direction.Down, priority = priorityDown });

        #endregion

        ProcessMoveWithTempList();

        tempList.Clear();
    }

    private void ProcessMoveWithTempList()
    {
        for (var i = 0; i < tempList.Count; i++)
        {
            var pos = tempList[i].pos;
            if (manager.IsIndexOutOfGridBound(pos.x, pos.y))
                continue;

            var entity = manager.GridCells.GetEntityAtIndex(pos.x, pos.y);
            if (entity != null && !(entity is BulletEntity))
                continue;

            Move(pos.x, pos.y);
        }
    }

    private void AddToPriorityTempList(MovePriority movePriority)
    {
        var player = manager.GameEntities.Player;
        if (player.IsGridIndexValid())
        {
            if (movePriority.pos.y <= player.GridIndexY)
                movePriority.priority += 100;
        }

        if (lastMovePos.HasValue)
        {
            var equal = movePriority.pos.Equals(lastMovePos.Value);
            if (equal)
                movePriority.priority += 100;
        }

        if (tempList.Count == 0)
            tempList.Add(movePriority);

        for (var i = 0; i < tempList.Count; i++)
        {
            var temp = tempList[i];
            if (temp.priority <= movePriority.priority)
                continue;

            tempList.Insert(i, movePriority);
            return;
        }

        tempList.Add(movePriority);
    }

    private void Move(int targetX, int targetY, bool triggerDelay = true)
    {
        if (delayDefaultRoutine != null)
            return; // during move delay.

        if (manager.IsIndexOutOfGridBound(targetX, targetY))
            return; // out of bound.

        var entity = manager.GridCells.GetEntityAtIndex(targetX, targetY);
        if (entity != null)
        {
            if (entity is BulletEntity bullet)
            {
                manager.GameEntities.DestroyBullet(bullet);
                OnBulletHit();
                return;
            }

            // cannot move over other things.
            return;
        }

        if (IsGridIndexValid())
            lastMovePos = new GridPos { x = gridIndexX, y = gridIndexY };

        manager.GridCells.SetEntityAtIndex(this, targetX, targetY);
        manager.GridSprites.RefreshSpriteUsage(spriteId, targetX, targetY);

        if (triggerDelay)
            StartDefaultDelay(manager.SpiderMoveSpeedCellPerSec);
    }
}
