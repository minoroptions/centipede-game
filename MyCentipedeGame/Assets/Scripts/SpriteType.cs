using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpriteType
{
    None = 0,
    BGTile,
    Bullet,
    CenBody,
    CenHead,
    MushHigh,
    MushLow,
    MushMed,
    Player,
    Spider,
}
